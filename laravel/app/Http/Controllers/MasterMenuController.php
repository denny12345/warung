<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use FILE;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Validator;

class MasterMenuController extends Controller
{
// ===================== Start Menu Utama =====================
    public function index_menu_utama(){
        return view('master_menu.menu_utama');
    }

    public function show_datatable_menu_utama(){
        try {
            $result = [];
            $count = 1;
            $query = DB::table('tm_menu')
                    ->where('tipe_menu','MENU UTAMA')
                    ->get();
            // dd($query);
            foreach ($query as $user) {
                $action_edit = '<center>
                                <a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon"
                                data-toggle="modal"
                                data-id= "'. $user->id.'"
                                data-target="#modal-edit" id="btn_update_menuUtama">
                                <span>
                                    <i class="la la-edit"></i>
                                    <span>Ubah</span>
                                </span>
                                </a>';

                $action_detail = '<a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete"
                                data-id="' . $user->id . '">
                                <span>
                                    <i class="la la-trash"></i>
                                    <span>Hapus</span>
                                </span>
                                </a> </center>';

<<<<<<< HEAD
=======
                $img_items  = '<img class="img-thumbnail" src="public/IMAGE_MENU_UTAMA/'.$user->image.'">';
>>>>>>> 006dcde2b74d16372520a9bfdb602b4e3c047814
                $data       = [];
                $data[]     = $count++;
                $data[]     = '<b>'.($user->nama).'</b>';
                $data[]     = '<b style="color:#a95e5e"> Rp. '.(number_format($user->harga, 2)).'</b>';
<<<<<<< HEAD
                $data[]     = ($user->image);
                $data[]     = '<b style="color:black"> Rp. '.(number_format($user->keuntungan, 2)).'</b>';
                $data[]     = $action_edit.' '.$action_detail ;
                $result[]   = $data;
=======
                $data[]     = $img_items;
                $data[]     = '<b style="color:black"> Rp. '.(number_format($user->keuntungan, 2)).'</b>';
                $data[]     = $action_edit.' '.$action_detail ;
                $result[]   = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function simpan_menu_utama(Request $request){

        $validator = \Validator::make($request->all(), [
            'nama'          => 'required',
            'harga'         => 'required',
            'foto_image'    => 'required',
            'keuntungan'    => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        if($request->foto_image == null){
            $request->foto_image == null;
        }else{
            $file_image = $request->file('foto_image');

            if ($file_image) {
                $file = $file_image->getClientOriginalName();
                $filepath = public_path('../../public/IMAGE_MENU_UTAMA');

                $upload = $file_image->move($filepath, $file);
                if ($upload) {
                    $request->foto_image = $file;
                }
            } else {
                $request->foto_image = null;
            }
        }

        DB::table('tm_menu')->insert([
                            'nama'         => $request->nama,
                            'harga'        => $request->harga,
                            'image'        => $request->foto_image,
                            'tipe_menu'    => 'MENU UTAMA',
                            'keuntungan'   => $request->keuntungan,
                            'created_at'   => \Carbon\Carbon::now()
                ]);
        // dd($query);
        return response()->json(['success'=>'Menu berhasil ditambahkan']);
    }

    public function AjaxDetailMenuUtama($id){
        $menu = \DB::table('tm_menu')
            ->select('*')
            ->where('tipe_menu','MENU UTAMA')
            ->where('id', $id)
            ->first();

        return response()->json(['status'=> 'success', 'result'=> $menu], 200);
    }

    public function update_menu_utama(Request $request){
        $rules = [
            'nama' => 'required',
            'harga' => 'required',
            'keuntungan' => 'required'

        ];
        $messages = [
            'required' => 'The :attribute is required.',
            'min' => 'The :attribute is lest than 3 character.',
        ];
        //validation roles
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->all()], 406);
        }

        $image = $request->file('image_menu');
        $query_check = \DB::table('tm_menu')
                        ->where('tipe_menu','MENU UTAMA')
                        ->where('id', $request->id)->first();
        if ($image) {
            if(File::exists(trim(public_path('../../public/IMAGE_MENU_UTAMA').'/'.$query_check->image))){
                    Storage::delete(trim(public_path('../../public/IMAGE_MENU_UTAMA').'/'.$query_check->image));
            }

            $menu_image = $image->getClientOriginalName();

            $filepath = public_path('../../public/IMAGE_MENU_UTAMA');

            $upload = $image->move($filepath, $menu_image);
            if ($upload) {
                $upload_db = $menu_image;
            }
        } else {
            $upload_db = $query_check->image;
        }

        try {
            DB::table('tm_menu')->where('id', $request->id)
                                    ->update([
                                            'nama'         => $request->nama,
                                            'harga'        => $request->harga,
                                            'tipe_menu'    => 'MENU UTAMA',
                                            'keuntungan'   => $request->keuntungan,
                                            'image'        => $upload_db,
                                            'updated_at'   => \Carbon\Carbon::now()
                                        ]);
            // dd($request->all());
            return response()->json(['status' => 'success', 'result' => 'Menu berhasil diubah'], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
        }
    }

    public function hapus_menu_utama(Request $request){
        try {
          $query_check = DB::table('tm_menu')
                        ->where('tipe_menu','MENU UTAMA')
          				->where('id', $request->id)
          				->first();

           Storage::delete((public_path('../../public/FILEUPLOAD').'/'.$query_check->image));
           DB::table('tm_menu')->where('id', '=', $request->id)->delete();

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Menu berhasil dihapus'], 200);
    }
    // ===================== End Menu Utama =====================

// ===================== Start Menu Pelengkap =====================
    public function index_menu_pelengkap()
    {
        return view('master_menu.menu_pelengkap');
    }

    public function show_datatable_menu_pelengkap()
    {
        try {
            $result = [];
            $count = 1;
            $query = DB::table('tm_menu')
            ->where('tipe_menu','=','MENU PELENGKAP')
            ->get();
            // dd($query);
            foreach ($query as $user) {
                $action_edit = '<center>
                                <a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon"
                                data-toggle="modal"
                                data-id= "' . $user->id . '"
                                data-target="#modal-edit" id="btn_update_menuPelengkap">
                                <span>
                                    <i class="la la-edit"></i>
                                    <span>Ubah</span>
                                </span>
                                </a>';

                $action_detail = '<a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete"
                                data-id="' . $user->id . '">
                                <span>
                                    <i class="la la-trash"></i>
                                    <span>Hapus</span>
                                </span>
                                </a> </center>';
                $img_items = '<img class="img-thumbnail" src="public/IMAGE_MENU_UTAMA/' . $user->image . '">';
                $data       = [];
                $data[]     = $count++;
                $data[]     = '<b>' . ($user->nama) . '</b>';
                $data[]     = '<b style="color:#a95e5e"> Rp. ' . (number_format($user->harga, 2)) . '</b>';
                $data[]     = $img_items;
                $data[]     = '<b style="color:black"> Rp. ' . (number_format($user->keuntungan, 2)) . '</b>';
                $data[]     = $action_edit . ' ' . $action_detail;
                $result[]   = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function simpan_menu_pelengkap(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'nama'          => 'required',
            'harga'         => 'required',
            'foto_image'    => 'required',
            'keuntungan'    => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        if ($request->foto_image == null) {
            $request->foto_image == null;
        } else {
            $file_image = $request->file('foto_image');

            if ($file_image) {
                $file = $file_image->getClientOriginalName();
                $filepath = public_path('../../public/IMAGE_MENU_UTAMA');

                $upload = $file_image->move($filepath, $file);
                if ($upload) {
                    $request->foto_image = $file;
                }
            } else {
                $request->foto_image = null;
            }
        }

        DB::table('tm_menu')->insert([
            'nama'         => $request->nama,
            'harga'        => $request->harga,
            'image'        => $request->foto_image,
            'tipe_menu'    => 'MENU PELENGKAP',
            'keuntungan'   => $request->keuntungan,
            'created_at'   => \Carbon\Carbon::now()
        ]);
        // dd($query);
        return response()->json(['success' => 'Menu berhasil ditambahkan']);
    }

    public function AjaxDetailMenuPelengkap($id)
    {
        $menu = \DB::table('tm_menu')
        ->select('*')
        ->where('tipe_menu','MENU PELENGKAP')
        ->where('id', $id)
        ->first();

        return response()->json(['status' => 'success', 'result' => $menu], 200);
    }

    public function update_menu_pelengkap(Request $request)
    {
        $rules = [
            'nama'       => 'required',
            'harga'      => 'required',
            'keuntungan' => 'required'

        ];
        $messages = [
                'required'  => 'The :attribute is required.',
                'min'       => 'The :attribute is lest than 3 character.',
            ];
        //validation roles
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->all()], 406);
        }

        $image = $request->file('image_menu');
        $query_check = \DB::table('tm_menu')
                        ->where('tipe_menu','MENU PELENGKAP')
                        ->where('id', $request->id)->first();
        if ($image) {
            if (File::exists(trim(public_path('../../public/IMAGE_MENU_UTAMA') . '/' . $query_check->image))) {
                Storage::delete(trim(public_path('../../public/IMAGE_MENU_UTAMA') . '/' . $query_check->image));
            }

            $menu_image = $image->getClientOriginalName();

            $filepath = public_path('../../public/IMAGE_MENU_UTAMA');

            $upload = $image->move($filepath, $menu_image);
            if ($upload) {
                $upload_db = $menu_image;
            }
        } else {
            $upload_db = $query_check->image;
        }

        try {
            DB::table('tm_menu')->where('id', $request->id)
            ->update([
                'nama'         => $request->nama,
                'harga'        => $request->harga,
                'tipe_menu'    => 'MENU PELENGKAP',
                'keuntungan'   => $request->keuntungan,
                'image'        => $upload_db,
                'updated_at'   => \Carbon\Carbon::now()
            ]);
            // dd($request->all());
            return response()->json(['status' => 'success', 'result' => 'Menu berhasil diubah'], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
        }
    }

    public function hapus_menu_pelengkap(Request $request)
    {
        try {
            $query_check = DB::table('tm_menu')
            ->where('tipe_menu','MENU PELENGKAP')
            ->where('id', $request->id)
            ->first();

            Storage::delete((public_path('../../public/FILEUPLOAD') . '/' . $query_check->image));
            DB::table('tm_menu')->where('id', '=', $request->id)->delete();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Menu berhasil dihapus'], 200);
    }
// ===================== End Menu Pelengkap =====================

// ===================== Start Minuman =====================
    public function index_menu_minuman(){
        return view('master_menu.menu_minuman');
    }

    public function show_datatable_menu_minuman(){
        try {
            $result = [];
            $count = 1;
            $query = DB::table('tm_menu')
                    ->where('tipe_menu','MENU MINUMAN')
                    ->get();
            // dd($query);
            foreach ($query as $user) {
                $action_edit = '<center>
                                <a href="#" class="btn btn-success btn-sm m-btn  m-btn m-btn--icon"
                                data-toggle="modal"
                                data-id= "'. $user->id.'"
                                data-target="#modal-edit" id="btn_update_menuMinuman">
                                <span>
                                    <i class="la la-edit"></i>
                                    <span>Ubah</span>
                                </span>
                                </a>';

                $action_detail = '<a href="#" class="btn btn-danger m-btn btn-sm m-btn m-btn--icon" id="btn-delete"
                                data-id="' . $user->id . '">
                                <span>
                                    <i class="la la-trash"></i>
                                    <span>Hapus</span>
                                </span>
                                </a> </center>';
                $img_items = '<img class="img-thumbnail" src="public/IMAGE_MENU_UTAMA/'.$user->image.'">';
                $data     = [];
                $data[]   = $count++;
                $data[]   = '<b>'.($user->nama).'</b>';
                $data[]   = '<b style="color:#a95e5e"> Rp. '.(number_format($user->harga, 2)).'</b>';
                $data[]   = $img_items;
                $data[]   = '<b style="color:black"> Rp. '.(number_format($user->keuntungan, 2)).'</b>';
                $data[]   = $action_edit.' '.$action_detail ;
                $result[] = $data;
>>>>>>> 006dcde2b74d16372520a9bfdb602b4e3c047814
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function simpan_menu_minuman(Request $request){

        $validator = \Validator::make($request->all(), [
            'nama'          => 'required',
            'harga'         => 'required',
            'foto_image'    => 'required',
            'keuntungan'    => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        if($request->foto_image == null){
            $request->foto_image == null;
        }else{
            $file_image = $request->file('foto_image');

            if ($file_image) {
                $file = $file_image->getClientOriginalName();
                $filepath = public_path('../../public/IMAGE_MENU_UTAMA');

                $upload = $file_image->move($filepath, $file);
                if ($upload) {
                    $request->foto_image = $file;
                }
            } else {
                $request->foto_image = null;
            }
        }

        DB::table('tm_menu')->insert([
                            'nama'         => $request->nama,
                            'harga'        => $request->harga,
                            'image'        => $request->foto_image,
                            'tipe_menu'    => 'MENU MINUMAN',
                            'keuntungan'   => $request->keuntungan,
                            'created_at'   => \Carbon\Carbon::now()
                ]);
        // dd($query);
        return response()->json(['success'=>'Menu berhasil ditambahkan']);
    }

    public function AjaxDetailMenuMinuman($id){
        $menu = \DB::table('tm_menu')
            ->select('*')
            ->where('tipe_menu','MENU MINUMAN')
            ->where('id', $id)
            ->first();

        return response()->json(['status'=> 'success', 'result'=> $menu], 200);
    }

    public function update_menu_minuman(Request $request){
        $rules = [
            'nama'       => 'required',
            'harga'      => 'required',
            'keuntungan' => 'required'

        ];
        $messages = [
            'required'  => 'The :attribute is required.',
            'min'       => 'The :attribute is lest than 3 character.',
        ];
        //validation roles
        $validator = \Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()->all()], 406);
        }

        $image = $request->file('image_menu');
        $query_check = \DB::table('tm_menu')
                        ->where('tipe_menu','MENU MINUMAN')
                        ->where('id', $request->id)->first();
        if ($image) {
            if(File::exists(trim(public_path('../../public/IMAGE_MENU_UTAMA').'/'.$query_check->image))){
                    Storage::delete(trim(public_path('../../public/IMAGE_MENU_UTAMA').'/'.$query_check->image));
            }

            $menu_image = $image->getClientOriginalName();

            $filepath = public_path('../../public/IMAGE_MENU_UTAMA');

            $upload = $image->move($filepath, $menu_image);
            if ($upload) {
                $upload_db = $menu_image;
            }
        } else {
            $upload_db = $query_check->image;
        }

        try {
            DB::table('tm_menu')->where('id', $request->id)
                                    ->update([
                                            'nama'         => $request->nama,
                                            'harga'        => $request->harga,
                                            'tipe_menu'    => 'MENU MINUMAN',
                                            'keuntungan'   => $request->keuntungan,
                                            'image'        => $upload_db,
                                            'updated_at'   => \Carbon\Carbon::now()
                                        ]);
            // dd($request->all());
            return response()->json(['status' => 'success', 'result' => 'Menu berhasil diubah'], 200);
        } catch (\Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()], 406);
        }
    }

    public function hapus_menu_minuman(Request $request){
        try {
          $query_check = DB::table('tm_menu')
                        ->where('tipe_menu','MENU MINUMAN')
                        ->where('id', $request->id)
                        ->first();

           Storage::delete((public_path('../../public/FILEUPLOAD').'/'.$query_check->image));
           DB::table('tm_menu')->where('id', '=', $request->id)->delete();

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 404);
        }
        return response()->json(['status' => 'success', 'result' => 'Menu berhasil dihapus'], 200);
    }
// ===================== End Minuman =====================

}
