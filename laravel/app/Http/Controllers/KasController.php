<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Validator;

class KasController extends Controller
{
    public function index(){
        return view('kas.index');
    }

    public function getDataKas(){
        try {
            $result = [];
            $count = 1;
            $query = DB::table('tt_kas')->get();
            
            foreach ($query as $user) {
                $data       = [];
                $data[]     = $count++;
                $data[]     = '<b>'.($user->created_at).'</b>';
                $data[]     = $user->status;
                $data[]     = '<b style="color:#a95e5e"> Rp. '.(number_format($user->jumlah_kas, 2)).'</b>';
                $data[]     = $user->keterangan;
                $result[]   = $data;
            }
            return response()->json(['result' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], 406);
        }
    }

    public function simpanKas(Request $request){

        $validator = \Validator::make($request->all(), [
            // 'jumlah_kas'	=> 'required',
            'keterangan'    => 'required',
            'status'    	=> 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $data = DB::table('tt_kas')->insert([
                            'generate_kode_kas' => 12345,
                            'jumlah_kas'		=> $request->jumlah_uang,
                            'keterangan'        => $request->keterangan,
                            'status'    		=> $request->status,
                            'created_at'   		=> \Carbon\Carbon::now()
                ]);
        dd($data);
        return response()->json(['success'=>'Kas Berhasil Ditambahkan']);
    }
}
