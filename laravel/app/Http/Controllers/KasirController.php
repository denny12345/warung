<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class KasirController extends Controller
{
    public function index(){
        $menu_utama = $this->showing_produk();
        return view('kasir.index', compact('menu_utama'));
    }

    public function showing_produk(){
        $query_menu_utama = DB::table('tm_menu')
                        ->select('*')
                        ->where('tipe_menu','MENU UTAMA')
                        ->get();
        return $query_menu_utama;
    }
}
