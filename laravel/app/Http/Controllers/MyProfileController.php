<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class MyProfileController extends Controller
{
    public function index()
    {
        return view('my_profile.index');
    }
}