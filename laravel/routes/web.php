<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'LoginController@index');

//login
Route::get('/', 'LoginController@index')->name('login')->middleware('access');
Route::post('post-login', 'LoginController@postLogin');
Route::match(['post', 'get'],'reg', 'LoginController@postRegister');


Route::group(['middleware' => ['auth', 'loguser']], function(){
// DASBOARD-----------------------------------------------------------------------------------------------------------------------
Route::get('dashboard', 'DashboardController@index');
Route::get('logout', 'LoginController@logout');

// MASTER MENU UTAMA---------------------------------------------------------------------------------------------------------------
Route::get('master-menu-utama', 'MasterMenuController@index_menu_utama');
Route::get('master-menu-datatable', 'MasterMenuController@show_datatable_menu_utama');
Route::post('simpan-menu-utama', 'MasterMenuController@simpan_menu_utama');
Route::get('detail-menu-utama/{id}', 'MasterMenuController@AjaxDetailMenuUtama');
Route::post('update-menu-utama', 'MasterMenuController@update_menu_utama');
Route::post('hapus-menu-utama', 'MasterMenuController@hapus_menu_utama');

// MASTER MENU TAMBAHAN------------------------------------------------------------------------------------------------------------
Route::get('master-menu-pelengkap', 'MasterMenuController@index_menu_pelengkap');
Route::get('master-menu-datatable-pelengkap', 'MasterMenuController@show_datatable_menu_pelengkap');
Route::post('simpan-menu-pelengkap', 'MasterMenuController@simpan_menu_pelengkap');
Route::get('detail-menu-pelengkap/{id}', 'MasterMenuController@AjaxDetailMenuPelengkap');
Route::post('update-menu-pelengkap', 'MasterMenuController@update_menu_pelengkap');
Route::post('hapus-menu-pelengkap', 'MasterMenuController@hapus_menu_pelengkap');

// MASTER MENU MINUMAN-------------------------------------------------------------------------------------------------------------
Route::get('master-menu-minuman', 'MasterMenuController@index_menu_minuman');
Route::get('master-menu-datatable-minuman', 'MasterMenuController@show_datatable_menu_minuman');
Route::post('simpan-menu-minuman', 'MasterMenuController@simpan_menu_minuman');
Route::get('detail-menu-minuman/{id}', 'MasterMenuController@AjaxDetailMenuMinuman');
Route::post('update-menu-minuman', 'MasterMenuController@update_menu_minuman');
Route::post('hapus-menu-minuman', 'MasterMenuController@hapus_menu_minuman');

// KAS-----------------------------------------------------------------------------------------------------------------------------
Route::get('kas', 'KasController@index');
Route::get('getKas', 'KasController@getDataKas');
Route::post('simpanKas', 'KasController@simpanKas');

// USER----------------------------------------------------------------------------------------------------------------------------
Route::resource('users','UserController');
Route::get('datatable_users', 'UserController@show_data');
Route::get('user/edit/{id}','UserController@edit');

// MyProfile-----------------------------------------------------------------------------------------------------------------------
Route::get('my-profile', 'MyProfileController@index');

// MyProfile-----------------------------------------------------------------------------------------------------------------------
Route::get('akun-users', 'AkunUsersController@index');

// KASIR---------------------------------------------------------------------------------------------------------------------------
Route::get('kasir', 'KasirController@index');


});
