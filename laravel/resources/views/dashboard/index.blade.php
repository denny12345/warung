@extends('template')
@section('konten')

<div class="m-grid__item m-grid__item--fluid m-wrapper" style="margin:10px">
<div class="m-portlet ">
							<div class="m-portlet__body  m-portlet__body--no-padding">
								<div class="row m-row--no-padding m-row--col-separator-xl">


                                        <div class="col-md-12 col-lg-6 col-xl-4">

                                            <!--begin::New Orders-->
                                            <div class="m-widget24">
                                                <div class="m-widget24__item">
                                                    <h4 class="m-widget24__title">
                                                        Keuntungan hari ini
                                                    </h4><br>
                                                    <span class="m-widget24__desc">
                                                        Total Keuntungan hari ini
                                                    </span>
                                                    <span class="m-widget24__stats m--font-success">
                                                        <b style="font-size: 19px">Rp. 4.500.000,00</b>
                                                    </span>
                                                    <div class="m--space-10"></div>
                                                    <div class="progress m-progress--sm">
                                                        <div class="progress-bar m--bg-success" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span class="m-widget24__change">
                                                        Total item yang terjual <b style="color: #c26d64">20 /items</b>
                                                    </span>

                                                </div>
                                            </div>

                                            <!--end::New Orders-->
                                        </div>
                                        <div class="col-md-12 col-lg-6 col-xl-4">

                                            <!--begin::New Users-->
                                            <div class="m-widget24">
                                                <div class="m-widget24__item">
                                                    <h4 class="m-widget24__title">
                                                       Keuntungan Bulanan
                                                    </h4><br>
                                                    <span class="m-widget24__desc">
                                                        Total Keuntungan Bulanan
                                                    </span>
                                                    <span class="m-widget24__stats m--font-warning">
                                                        <b style="font-size: 19px">Rp. 98.000.000,00</b>
                                                    </span>
                                                    <div class="m--space-10"></div>
                                                    <div class="progress m-progress--sm">

                                                        <div class="progress-bar m--bg-warning" role="progressbar" style="width:90" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span class="m-widget24__change">
                                                        Total item yang terjual <b style="color: #c26d64">200 /items</b>
                                                    </span>

                                                </div>
                                            </div>

                                            <!--end::New Users-->
                                        </div>
                                        <div class="col-md-12 col-lg-6 col-xl-4">

                                            <!--begin::New Users-->
                                            <div class="m-widget24">
                                                <div class="m-widget24__item">
                                                    <h4 class="m-widget24__title">
                                                        Keuntungan tahunan
                                                    </h4><br>
                                                    <span class="m-widget24__desc">
                                                        Total Keuntungan tahunan
                                                    </span>
                                                    <span class="m-widget24__stats m--font-danger">
                                                        <b style="font-size: 19px">Rp. 450.000.000,00</b>
                                                    </span>

                                                    <div class="m--space-10"></div>
                                                    <div class="progress m-progress--sm">
                                                        <div class="progress-bar m--bg-danger" role="progressbar" style="width: 100" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span class="m-widget24__change">
                                                        Total item yang terjual <b style="color: #c26d64">10110 /items</b>
                                                    </span>

                                                </div>
                                            </div>

                                            <!--end::New Users-->
                                        </div>

								</div>
							</div>
</div>

<div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Activity
                </h3>
            </div>
        </div>

    </div>
    <div class="m-portlet__body">
        <div id="graph_dashboard"></div>
    </div>
</div>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script>
    $(document).ready(function(){

            Highcharts.chart('graph_dashboard', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Grafik'
                },
                subtitle: {
                    text: 'Source: <a href="">http://localhost/quisioner_alumni/public/</a>'
                },
                xAxis: {

                    categories: ['Harian',
                                'Bulanan',

                                ],
                    title: {
                        text: null
                    }
                },


                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total ',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ' Data'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'horizontal',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                    shadow: true
                },
                credits: {
                    enabled: false
                },

                series: [
                         {
                            name: 'Sudah Mengisi',
                            data: [120],
                            color: '#ffb822 '
                        }, {
                            name: 'Belum Mengisi',
                            data: [90],
                            color: '#f4516c'
                        }
                                      ]

                });


    });
</script>
@endsection
