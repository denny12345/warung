@extends('template')
@section('konten')
<head>
    <link href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <style>
        .modal.modal-wide{
            overflow: hidden;
        }

        .modal-wide .modal-body {
            overflow-y: auto;
        }

@-webkit-keyframes custome-animate {
	0% {
		-webkit-transform-style: preserve-3d;
		-moz-transform-style: preserve-3d;
		transform-style: preserve-3d;
		-webkit-transform:  perspective(90%) rotateY(-65deg) rotateX(-45deg) translateZ(-200px);
		-moz-transform: perspective(90%) rotateY(-65deg) rotateX(-45deg) translateZ(-200px);
		-ms-transform: perspective(90%) rotateY(-65deg) rotateX(-45deg) translateZ(-200px);
		transform: perspective(90%) rotateY(-65deg) rotateX(-45deg) translateZ(-200px);
		opacity: 0;
	}
    68% {
		-webkit-transform:  rotateY(10deg) rotateX(10deg) translateZ(20px);
		-moz-transform:  rotateY(10deg) rotateX(10deg) translateZ(20px);
		-ms-transform:  rotateY(10deg) rotateX(10deg) translateZ(20px);
		transform:  rotateY(10deg) rotateX(10deg) translateZ(20px);
		opacity: 0.8;
    }
	100% {
		-webkit-transform:  rotateY(0deg) rotateX(0deg) translateZ(0px);
		-moz-transform:  rotateY(0deg) rotateX(0deg) translateZ(0px);
		-ms-transform:  rotateY(0deg) rotateX(0deg) translateZ(0px);
		transform:  rotateY(0deg) rotateX(0deg) translateZ(0px);
		opacity: 1;
	}
}
@keyframes custome-animate {
	0% {
		-webkit-transform-style: preserve-3d;
		-moz-transform-style: preserve-3d;
		transform-style: preserve-3d;
		-webkit-transform: perspective(90%) rotateY(-65deg) rotateX(-45deg) translateZ(-200px);
		-moz-transform: perspective(90%) rotateY(-65deg) rotateX(-45deg) translateZ(-200px);
		-ms-transform: perspective(90%) rotateY(-65deg) rotateX(-45deg) translateZ(-200px);
		transform: perspective(90%) rotateY(-65deg) rotateX(-45deg) translateZ(-200px);
		opacity: 0;
	}
	68% {
		-webkit-transform:  rotateY(10deg) rotateX(10deg) translateZ(20px);
		-moz-transform:  rotateY(10deg) rotateX(10deg) translateZ(20px);
		-ms-transform:  rotateY(10deg) rotateX(10deg) translateZ(20px);
		transform:  rotateY(10deg) rotateX(10deg) translateZ(20px);
		opacity: 0.8;
    }
    100% {
		-webkit-transform: rotateY(0deg) rotateX(0deg) translateZ(0px);
		-moz-transform:  rotateY(0deg) rotateX(0deg) translateZ(0px);
		-ms-transform:  rotateY(0deg) rotateX(0deg) translateZ(0px);
		transform:  rotateY(0deg) rotateX(0deg) translateZ(0px);
		opacity: 1;
	}
}
.custome-animate {
    -webkit-animation-duration: 0.75s;
    animation-duration: 0.75s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
	-webkit-animation-name: custome-animate;
	animation-name: custome-animate;
}



/********Demo 2*******/

.opacity-animate{
    animation:opt-animation 0.7s;
     -webkit-animation-fill-mode: forwards;
    animation-fill-mode: forwards;


}


@-webkit-keyframes opt-animation {
  0%   { opacity: 0; }
  100% { opacity: 1; }
}
@-moz-keyframes opt-animation {
  0%   { opacity: 0; }
  100% { opacity: 1; }
}
@-o-keyframes opt-animation {
  0%   { opacity: 0; }
  100% { opacity: 1; }
}
@keyframes opt-animation {
  0%   { opacity: 0;}
  100% { opacity: 1;}
}

/***** Demo 3 *********/
.opacity-animate3{
    animation:opt-animation3 1s ;
  -moz-animation-fill-mode: forwards
    -webkit-animation-fill-mode: forwards;
    animation-fill-mode: forwards;
}

@-webkit-keyframes opt-animation3 {
  0%   { opacity: 0; transform: scale(0.75);}
  100% { opacity: 1; transform: scale(1);}
}
@-moz-keyframes opt-animation3 {
   0%   { opacity: 0; transform: scale(0.75);}
  100% { opacity: 1; transform: scale(1);}
}
@-o-keyframes opt-animation3{
  0%   { opacity: 0; transform: scale(0.75);}
  100% { opacity: 1; transform: scale(1);}
}
@keyframes opt-animation3 {
  0%   { opacity: 0; transform: scale(0.75);}
  100% { opacity: 1; transform: scale(1);}
}

/* .hide-opacity{
   animation:scaleme 1s;
}

@-webkit-keyframes scaleme {
  0% { -webkit-transform: scale(1); opacity: 1; }
  100% { -webkit-transform: scale(0); opacity: 0;display:none;}
}
 */



/* .demo4
 &.fade {
  .modal-dialog {
   .translate(0, 0);
  }
 }
}
/* or */
/* .demo4.fade .modal-dialog {
 .translate(0, 0);
}
 */
.opacity-animate4{
    animation:opt-animation4 1s;
  animation-fill-mode: forwards
}


@keyframes opt-animation4 {
  0% {
    transform: scale(0) rotate(360deg);
  }
  60% {
    transform: scale(0.8) rotate(-10deg);
  }
  100% {
    transform: scale(1) rotate(0deg);
  }
}
    </style>
</head>


<div class="m-content" style="padding: 15px;margin-top:-10px;margin-left:-10px">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="flaticon-statistics"></i>
                            </span>
                            <b style="color: red">* info :</b>&nbspJika ingin menambahkan item makanan klik tombol <b style="color: #4586c8">&nbspTambah Menu Minuman</b>
                            <h2 class="m-portlet__head-label m-portlet__head-label--primary" style="width: 230px;background-color:#368adf">
                                <span>
                                    <i class="la la-list"></i>

                                </span> <span style="font-size: 1rem">Daftar Menu Minuman</span>
                            </h2>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">

                        </ul>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" data-toggle="modal" data-target="#CreateMenuMinumanModal" class="btn btn-warning m-btn m-btn--icon m-btn--air">
                                        <span>
                                            <i class="la la-plus"></i>
                                            <span><b>Tambah Menu Minuman</b></span>
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <br>
                <div class="m-portlet__body">

                    <table cellpadding="5" class="table-striped tabel_show table nowrap" cellspacing="0" width="100%">
                        <thead style="background-color: #368adf;color: #e5f6dd;border-radius:10;">
                            <tr>
                                <th>No.</th>
                                <th>Nama Menu</th>
                                <th>Harga</th>
                                <th>Gambar</th>
                                <th style="width: 15%">Keuntungan</th>
                                <th style="width: 10%">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <!--end::Portlet-->
        </div>

    </div>
</div>
    {{--  input data modal  --}}
    <div class="modal opacity-animate" id="CreateMenuMinumanModal">
        <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 50%;overflow-y: initial !important">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="background-color: #368adf;color:white">
                    <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-list"></i> INPUT MENU MINUMAN</b></h6>
                    <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="height: 390px;
                overflow-y: auto;">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success!</strong>Product was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <hr>
                    <form id="form_submit_menu_minuman" method="post">
                        @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name"* >Nama Menu:</label>
                                    </div>
                                    <div class="col-md-8">
                                       <input required type="text" class="form-control" placeholder="Nama Menu" name="nama">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name"* >Harga:</label>
                                    </div>
                                    <div class="col-md-8">
                                       <input required type="text" class="form-control" placeholder="Harga.." name="harga">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">* Keuntungan:</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input required type="text" class="form-control" placeholder="Keuntungan.." name="keuntungan">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="Name">* Upload Gambar Menu:</label><br>
                                        <small style="color: red"><b>* Format upload JPG atau PNG</b></small>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="custom-file clear_upload">
                                            <input required type="file" class="custom-file-input clear_upload" id="file_upload_data" name="foto_image">
                                            <label class="custom-file-label" for="customFile">Masukkan gambar menu disini</label>
                                        </div>
                                        <p id="error_photo1" style="display:none; color:#FF0000;">
                                            Format file yang diupload harus PDF,PNG atau JPG !
                                           </p>
                                           <p id="error_photo2" style="display:none; color:#FF0000;">
                                           Maximum File Size Limit is 1MB.
                                       </p>
                                     <br>
                                     <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                        {{-- <input type="file" name="file_upload" id="myPdf" accept="image/PNG, image/png, image/jpg,image/JPG, image/jpeg, image/JPEG"/><br> --}}
                                       <br><br>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>


                </div>
                <!-- Modal footer -->
                <div class="modal-footer" style="height: 70px">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                </div>
            </form>
            </div>
        </div>
    </div>

    {{--  edit data modal  --}}
    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog modal-lg opacity-animate2" style="max-width: 50%;overflow-y: initial !important">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="background-color: #4f7f4f;color:white">
                    <h6 class="modal-title" style="color: whitesmoke;height:5px;padding-bottom:7px;margin-top:-10px"><b><i class="m-menu__link-icon flaticon-list"></i> UPDATE MENU MINUMAN</b></h6>
                    <button type="button" class="close" style="color: whitesmoke" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="height: 420px;
                overflow-y: auto;">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success!</strong>Product was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <hr>
                    <form id="form_edit" enctype="multipart/form-data" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" id="txtedit_id"  class="form-control" name="id">
                        <div class="row">

                            <div class="col-md-12">

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name"* >Nama Menu:</label>
                                        </div>
                                        <div class="col-md-8">
                                           <input id="txtedit_nama" required type="text" class="form-control" placeholder="Nama Menu.." name="nama">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name"* >Harga:</label>
                                        </div>
                                        <div class="col-md-8">
                                           <input id="txtedit_harga" required type="text" class="form-control" placeholder="Harga.." name="harga">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">* Keuntungan:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input id="txtedit_keuntungan" required type="text" class="form-control" placeholder="Keuntungan.." name="keuntungan">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="Name">* Upload Gambar Menu:</label><br>
                                            <small style="color: red"><b>* Format upload JPG atau PNG</b></small>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="custom-file clear_upload">
                                                <input required type="file" class="custom-file-input clear_upload" id="txtedit_file_menu_utama" name="image_menu">
                                                <label class="custom-file-label" for="customFile">Masukkan gambar menu disini</label>
                                            </div>
                                            <p id="error_photo1" style="display:none; color:#FF0000;">
                                                Format file yang diupload harus PDF,PNG atau JPG !
                                               </p>
                                               <p id="error_photo2" style="display:none; color:#FF0000;">
                                               Maximum File Size Limit is 1MB.
                                           </p>
                                         <br>
                                         <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                            {{-- <input type="file" name="file_upload" id="myPdf" accept="image/PNG, image/png, image/jpg,image/JPG, image/jpeg, image/JPEG"/><br> --}}
                                           <br><br>

                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>


                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" id="SubmitCreateForm">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                </div>
            </form>
            </div>
        </div>
    </div>
    {{--  modal detail  --}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

<script>
    $(document).ready(function () {
        var table_menu = $('.tabel_show').DataTable({
            responsive:true,
            paging: true,
            info: true,
            searching: true,
            scrollX:        true,
            scrollCollapse: true,
            autoWidth:         true,
            "aaSorting": [],
            "ordering": true,
            ajax: {
                url: '{{url("master-menu-datatable-minuman")}}',
                dataSrc: 'result',
            },

        });

        //input data

        $('#form_submit_menu_minuman').submit(function (event) {
            event.preventDefault();
            $('#CreateMenuUtamaModal').modal('hide');
            var url = '{{ url('simpan-menu-minuman') }}';
            ajaxProcessInsert(url, new FormData(this))
        });

        function ajaxProcessInsert(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response) {
                        console.log(response);
                        $('.cssload-container').hide();
                        $('#form_submit_menu_minuman')[0].reset();
                        $('.tabel_show').DataTable().ajax.reload();
                        Swal.fire({
                            title: 'Information',
                            html: 'Menu Minuman berhasil disimpan',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.errors;
                            if (Array.isArray(message)) {
                                console.log(message);
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        //action edit

        $('#form_edit').submit(function (event) {
            $('#modal-edit').modal('hide');
            event.preventDefault();
            var url = '{{ url('update-menu-minuman') }}';
            ajaxProcess(url, $(this).serialize())
        });

        function ajaxProcess(url, data) {
            $.ajax({
                type: 'post',
                url: url,
                data: data,
                dataType : 'json',
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    console.log(response);
                    if (response) {
                        $('.cssload-container').hide();

                        $('.tabel_show').DataTable().ajax.reload();


                        Swal.fire({
                            title: 'Information',
                            html: '<strong>' + response.result + '</strong>',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('#form_edit')[0].reset();
                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        };

        $('.tabel_show tbody').on('click', '#btn_update_menuMinuman', function (e) {
            var id = $(this).attr('data-id');
            var url = '{!! url('detail-menu-minuman') !!}/'+id;
            ajaxDetailMenuMinuman(url);
        });

        function ajaxDetailMenuMinuman(url){
            $.ajax({
                type: 'GET',
                url: url,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    $('.cssload-container').hide();
                    if(response.status == 'success') {
                       if(response.result != null){
                           var data = response.result;
                           $('#txtedit_id').val(data.id);
                           $('#txtedit_nama').val(data.nama);
                           $('#txtedit_harga').val(data.harga);
                           $('#txtedit_keuntungan').val(data.keuntungan);
                           $('#modal-edit').modal('show');
                       }
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.message;
                            if (Array.isArray(message)) {
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })

                }

            })
        }

        //modal detail jquery

        //delete data propinsi

        $('.tabel_show tbody').on('click', '#btn-delete', function () {
            var url = '{{ url('hapus-menu-minuman') }}';
            var id = $(this).attr('data-id');
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var data_id = {_token: CSRF_TOKEN, id: id};
            delete_master(url, data_id);
        });


        function delete_master(url, data) {
            swal.fire({
                title: "Informasi !",
                text: "Apakah anda ingin menghapus menu minuman ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Ya, Hapus Menu!'
            }).then((result) => {
                if(result.value)
                {
                    $.ajax({
                    type: "POST",
                    url: url,
                    /* send the csrf-token and the input to the controller */
                    data: data,
                    dataType: "json",
                    /* remind that 'data' is the response of the AjaxController */
                    beforeSend: function () {
                        $('.cssload-container').show();
                    },
                    success: function (data) {
                        $('.cssload-container').hide();
                        $('.tabel_show').DataTable().ajax.reload();

                        swal.fire({
                            title: "Delete!",
                            text: data.result,
                            type: data.status,
                            button: false,
                            timer: 2000,
                        })
                    },
                    error: function (error) {
                        if (error) {
                            console.log(error);
                            var result = error.responseJSON;
                            if (result != null) {
                                var message = result.message;
                                if (Array.isArray(message)) {
                                    $.map(message, function (value, index) {
                                        message = value + '</br>';
                                    });
                                }
                            } else {
                                message = 'look like something when wrong';
                            }
                        } else {
                            message = 'look like something when wrong';
                        }

                        $('.cssload-container').hide();
                        Swal.fire({
                            title: 'Warning',
                            html: '<strong>' + message + '</strong>',
                            type: 'warning',
                            showConfirmButton: false,
                            timer: 3000
                        })

                    }

                });
                }
            });
        }

    })
</script>

@endsection
