@extends('template')
@section('konten')
<head>
    <style>
        @speed: .3s;
@speed1: .1s;
@speed2: 10s;
@corner-color: rgba(255,0,0,1);

.Scale(@scale:1){
  -webkit-transform: scale(@scale);
  -moz-transform: scale(@scale);
  -ms-transform: scale(@scale);
  -o-transform: scale(@scale);
  transform: scale(@scale);
}
.Transition(@vit:@speed , @ease:ease-in-out){
  -webkit-transition: all @vit @ease ;
  -moz-transition: all @vit @ease ;
  -ms-transition: all @vit @ease ;
  -o-transition: all @vit @ease ;
  transition: all @vit @ease;
}



#content {
  min-height: 400px;
  display: inline-block;
}

.product-grid {
  width: 100%;
  color: red;
  background-color: #eeeded;
  float: left;


}

.product-card {
  float: left;
   margin: 0 auto;
  position: block;
  width: 170px;
  height: 195px;
  display: inline-block;
  color: black;
  font-weight: bold;

}

.card {
  overflow: hidden;
  background-color: #ffffff;
  border-radius: 2px;
  display: block;
  height: 180px;
  position: relative;
  width: 140px;
  margin-top: 5px;
  margin-left: auto;
  margin-bottom: auto;
  margin-right: auto;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  .Transition();
}

.card:hover {
  box-shadow: 0 20px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);

}
.thumb {
  position: absolute;
  width:100% ;
  text-align: center;
  height: 160px;
  overflow: hidden;
}
.thumb img {
.Transition(@speed,ease-in-out);

  max-height: 174px;
    max-width: 150px;
    height:150px
  }
 .card:hover .thumb img{
   .Transition(@speed2,ease-out);
     max-height: 150px;
    max-width: 150px;
   opacity: 0.9;
   .Scale(3);

  }
.thumb-blur {
  position: absolute;
  top: 100px;
  width: 166px;
  text-align: center;
  height: 50px;
  overflow: hidden;
}
.thumb-blur img {
  filter: blur(0px);
.Transition(@speed1 ,ease-in);

  max-height: 146px;
    max-width: 146px;
  transform: translate(0px,-100px);
  opacity: 0;
  }
.card:hover .thumb-blur img {
  filter: blur(2px);
  opacity: 1;
   max-height: 150px;
    max-width: 140px;

}
.corner {

  border: 1px solid rgba(255,0,0,1);
  width: 100px;
  height: 20px;
  position: absolute;
  text-align: center;
  background-color: white;
  top: 10px;
  right: -25px;
  transform: rotate(45deg);
  z-index: 1;
  box-shadow: 0 5px 8px rgba(0,0,0,0.3);

  .Transition();

}
.corner p {
  font-size: 17px;
  padding: 0;
  margin: 0;
  margin-top: -2px;
  color: rgba(0,0,0,0.5);
}

.card:hover .corner {
      box-shadow: 0 4px 7px rgba(0,0,0,0.2);
      background-color: rgba(217, 224, 107, 0.8);
  }
.body-card {
 background-color: rgba(255,255,255,1);
  position: absolute;
  top: 150px;
  height: 100px;
  width: 100%;
  box-shadow: 0px -10px 10px rgba(0,0,0,0.2);
  .Transition(@speed,cubic-bezier(0,.81,.38,1.37));


  z-index: 1;

}
.card:hover .body-card {
  top: 100px;
  box-shadow: 0 -4px 7px rgba(0,0,0,0.1) ;
 background-color: rgba(255,255,255,0.5);
}

.body-header {
  position: absolute;
  width: 100%;
  height: 50px;
text-align: center;
vertical-align: middle;
  padding-top : 0;
}

.body-footer {
position: absolute;
display: inline-block;
  text-align: center;
  width: 100%;
  top: 48px;
  height: 52px;
  margin: 0;
  padding: 0;
  transition: all @speed ease-in-out;
}
.card:hover .body-footer {
  top: 40px;
  height: 60px;
}
.btn-warning.btn-xs{
 transform: scale(0.9);


}

div.ex1 {
  width: 745px;
  height: 410px;
  overflow: scroll;
}
div.ex2 {
  width: 500px;
  height: 260px;
  overflow: scroll;
}


.card:hover .body-header .name {
  color: rgba(0,0,0,1);
 transform: scale(1.1);

}.card:hover .body-header p {
  color: rgba(0,0,0,0.8);
 transform: scale(1.2);
}
.footer_button {
position: absolute;
right: 14px;
bottom: 29px;
left: 15px;
background-color: white;
text-align: center;
padding-top: 0px;
}
.pen body {
	padding-top:50px;
}

/* MODAL FADE LEFT RIGHT BOTTOM */
.modal.fade:not(.in).left .modal-dialog {
	-webkit-transform: translate3d(-0%, 0, 0);
	transform: translate3d(-0%, 0, 0);
}
.modal.fade:not(.in).right .modal-dialog {
	-webkit-transform: translate3d(0%, 0, 0);
	transform: translate3d(0%, 0, 0);
}
.modal.fade:not(.in).bottom .modal-dialog {
	-webkit-transform: translate3d(0, 0%, 0);
	transform: translate3d(0, 0%, 0);
}

.modal.right .modal-dialog {
	position:absolute;
	top:0;
	right:0;
	margin:0;
}

.modal.left .modal-dialog {
	position:absolute;
	top:0;
	left:0;
	margin:0;
}

.modal.left .modal-dialog.modal-sm {
	max-width:500px;
}

.modal.left .modal-content, .modal.right .modal-content {
	min-height:100vh;
	border:0;
}

</style>
</head>

<div class="row">
    <div class="m-content">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="m-card-profile">
                            <div class="m-card-profile__title m--hide">
                                Your Profile
                            </div>
                            <div class="m-card-profile__pic">
                                <div class="m-card-profile__pic-wrapper">
                                    <img src="assets/app/media/img/users/user4.jpg" alt="" />
                                </div>
                            </div>
                            <div class="m-card-profile__details">
                                <span class="m-card-profile__name">Mark Andre</span>
                                <a href="" class="m-card-profile__email m-link">mark.andre@gmail.com</a>
                            </div>
                        </div>
                        <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                            <li class="m-nav__separator m-nav__separator--fit"></li>
                            <li class="m-nav__section m--hide">
                                <span class="m-nav__section-text">Section</span>
                            </li>
                            <li class="m-nav__item">
                                <a href="header/profile&amp;demo=default.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-profile-1"></i>
                                    <span class="m-nav__link-title">
                                        <span class="m-nav__link-wrap">
                                            <span class="m-nav__link-text">My Profile</span>
                                            <span class="m-nav__link-badge"><span class="m-badge m-badge--success">2</span></span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="header/profile&amp;demo=default.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-share"></i>
                                    <span class="m-nav__link-text">Activity</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="header/profile&amp;demo=default.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-chat-1"></i>
                                    <span class="m-nav__link-text">Messages</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="header/profile&amp;demo=default.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-graphic-2"></i>
                                    <span class="m-nav__link-text">Sales</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="header/profile&amp;demo=default.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-time-3"></i>
                                    <span class="m-nav__link-text">Events</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="header/profile&amp;demo=default.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                    <span class="m-nav__link-text">Support</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                        <i class="flaticon-share m--hide"></i>
                                        Update Profile
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_user_profile_tab_1">
                            <form class="m-form m-form--fit m-form--label-align-left">
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <div class="col-10 ml-auto">
                                            <h3 class="m-form__section">Profil</h3>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Nama Lengkap</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="Mark Andre">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Jabatan</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="CTO">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Nama Perusahaan</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="Keenthemes">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">No. Telepon/HP</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="+456669067890">
                                        </div>
                                    </div>
                                    <!-- <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div> -->
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2">
                                            </div>
                                            <div class="col-7">
                                                <button type="reset" class="btn btn-accent m-btn m-btn--air m-btn--custom">Update</button>&nbsp;&nbsp;
                                                <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Batal</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script>
</script>

@endsection
