@extends('template')
@section('konten')
<div class="m-portlet">
    <div class="m-portlet__body m-portlet__body--no-padding">
        <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-md-12 col-lg-12 col-xl-4">

                <!--begin:: Widgets/Stats2-1 -->
                <div class="m-widget1">
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">KAS</h3>
                                <span class="m-widget1__desc">Total Uang Kas keseluruhan</span>
                            </div>
                            <div class="col m--align-right">
                                <span class="m-widget1__number m--font-brand" style="font-size: 18px"><b>Rp. 119.000.000,00</b></span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">Pengeluaran terakhir</h3>
                                <span class="m-widget1__desc">Jumlah Pengeluaran</span>
                            </div>
                            <div class="col m--align-right">
                                <span class="m-widget1__number m--font-danger" style="font-size: 18px"><b>- Rp. 25.000.000,00</b></span>
                            </div>
                        </div>
                    </div>
                    <div class="m-widget1__item">
                        <div class="row m-row--no-padding align-items-center">
                            <div class="col">
                                <h3 class="m-widget1__title">Pemasukkan terakhir</h3>
                                <span class="m-widget1__desc">Jumlah pemasukkan</span>
                            </div>
                            <div class="col m--align-right">
                                <span class="m-widget1__number m--font-success" style="font-size: 18px"><b>+ Rp.&nbsp15.0000.000,00</b></span>
                            </div>
                        </div>
                    </div>
                </div>

                <!--end:: Widgets/Stats2-1 -->
            </div>
            <div class="col-md-12 col-lg-12 col-xl-8">

                <!--begin:: Widgets/Stats2-2 -->
                <form id="form_submit_kas" method="post">
                    @csrf
                <div class="m-widget1">
                   <h5><b>Form Input Kas</b></h5>
                   <hr>
                    <div class="row">
                        <div class="col-md-2">
                            Jumlah uang
                        </div>
                        <div class="col-md-10">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text" id="basic-addon2">Rp.</span></div>
                                <input name="jumlah_uang" type="text" class="form-control m-input" placeholder="10.000.000">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-2">
                            Status kas
                        </div>
                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label class="m-option m-option--plain">
                                        <span class="m-option__control">
                                            <span class="m-radio m-radio--brand">
                                                <input type="radio" name="status" value="Pemasukan">
                                                <span></span>
                                            </span>
                                        </span>
                                        <span class="m-option__label">
                                            <span class="m-option__head">
                                                <span class="m-option__title">
                                                    Pemasukan
                                                </span>
                                            </span>
                                        </span>
                                    </label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="m-option m-option--plain">
                                        <span class="m-option__control">
                                            <span class="m-radio m-radio--brand">
                                                <input type="radio" name="status" value="Pengeluaran">
                                                <span></span>
                                            </span>
                                        </span>
                                        <span class="m-option__label">
                                            <span class="m-option__head">
                                                <span class="m-option__title">
                                                    Pengeluaran
                                                </span>
                                            </span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            Keterangan
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <textarea class="form-control m-input" name="keterangan" id="" cols="25" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary btn-block"><span>
                                <i class="la la-cart-plus"></i>
                            </span>PROSES KAS</button>
                        </div>
                    </div>
                </div>
                </form>
                <!--begin:: Widgets/Stats2-2 -->
            </div>

        </div>
    </div>
</div>


<div class="m-content" style="padding: 15px;margin-top:-10px;margin-left:-15px; margin-right: -15px;">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <!-- <div class="m-portlet__head-title"> -->
                            <span class="m-portlet__head-icon m--hide">
                                <!-- <i class="flaticon-statistics"></i> -->
                            </span>

                            <h2 class="m-portlet__head-label m-portlet__head-label--primary" style="width: 360px;background-color:#df5d7b">
                                <span>
                                    <i class="la la-list"></i>
                                </span>
                                <span>Rincian Pemasukkan & Pengeluaran</span>
                            </h2>
                        <!-- </div> -->
                    </div>
                </div>
               
                <div class="m-portlet__body">
                    <table cellpadding="5" class="table table-condensed table-hover tabel_show" cellspacing="0" width="100%">
                        <thead style="background-color: #368adf;color: #e5f6dd;border-radius:10;">
                            <tr>
                                <th>No.</th>
                                <th>Tanggal</th>
                                <th>Pengeluaran/Pemasukkan</th>
                                <th>Jumlah</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <!--end::Portlet-->
        </div>

    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script>
    $(document).ready(function () {
        $('.tabel_show').DataTable({
            responsive:true,
            paging: true,
            info: true,
            searching: true,
            scrollX: true,
            scrollCollapse: true,
            autoWidth: true,
            "aaSorting": [],
            "ordering": true,
            ajax: {
                url: '{{url("getKas")}}',
                dataSrc: 'result',
            },
        });

        $('#form_submit_kas').submit(function (event) {
            event.preventDefault();
            // alert('OKE');
            var url = '{{ url('simpanKas') }}';
            ajaxProcessInsert(url, new FormData(this));
        });

        function ajaxProcessInsert(url, data) {
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                cache: false,
                processData:false,
                contentType: false,
                beforeSend: function () {
                    $('.cssload-container').show();
                },
                success: function (response) {
                    if (response) {
                        console.log(response);
                        $('.cssload-container').hide();
                        $('#form_submit_kas')[0].reset();
                        $('.tabel_show').DataTable().ajax.reload();
                        Swal.fire({
                            title: 'Information',
                            html: 'Kas berhasil disimpan',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (error) {
                    if (error) {
                        console.log(error);
                        var result = error.responseJSON;
                        if (result != null) {
                            var message = result.errors;
                            if (Array.isArray(message)) {
                                console.log(message);
                                $.map(message, function (value, index) {
                                    message = value + '</br>';
                                });
                            }
                        } else {
                            message = 'look like something when wrong';
                        }
                    } else {
                        message = 'look like something when wrong';
                    }

                    $('.cssload-container').hide();
                    Swal.fire({
                        title: 'Warning',
                        html: '<strong>' + message + '</strong>',
                        type: 'warning',
                        showConfirmButton: false,
                        timer: 3000
                    })
                }
            })
        };
    });
</script>
@endsection