@extends('template')
@section('konten')
<head>
    <style>
        @speed: .3s;
@speed1: .1s;
@speed2: 10s;
@corner-color: rgba(255,0,0,1);

.Scale(@scale:1){
  -webkit-transform: scale(@scale);
  -moz-transform: scale(@scale);
  -ms-transform: scale(@scale);
  -o-transform: scale(@scale);
  transform: scale(@scale);
}
.Transition(@vit:@speed , @ease:ease-in-out){
  -webkit-transition: all @vit @ease ;
  -moz-transition: all @vit @ease ;
  -ms-transition: all @vit @ease ;
  -o-transition: all @vit @ease ;
  transition: all @vit @ease;
}



#content {
  min-height: 400px;
  display: inline-block;
}

.product-grid {
  width: 100%;
  color: red;
  background-color: #eeeded;
  float: left;


}

.product-card {
  float: left;
   margin: 0 auto;
  position: block;
  width: 170px;
  height: 195px;
  display: inline-block;
  color: black;
  font-weight: bold;

}

.card {
  overflow: hidden;
  background-color: #ffffff;
  border-radius: 2px;
  display: block;
  height: 180px;
  position: relative;
  width: 140px;
  margin-top: 5px;
  margin-left: auto;
  margin-bottom: auto;
  margin-right: auto;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  .Transition();
}

.card:hover {
  box-shadow: 0 20px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);

}
.thumb {
  position: absolute;
  width:100% ;
  text-align: center;
  height: 160px;
  overflow: hidden;
}
.thumb img {
.Transition(@speed,ease-in-out);

  max-height: 174px;
    max-width: 150px;
    height:150px
  }
 .card:hover .thumb img{
   .Transition(@speed2,ease-out);
     max-height: 150px;
    max-width: 150px;
   opacity: 0.9;
   .Scale(3);

  }
.thumb-blur {
  position: absolute;
  top: 100px;
  width: 166px;
  text-align: center;
  height: 50px;
  overflow: hidden;
}
.thumb-blur img {
  filter: blur(0px);
.Transition(@speed1 ,ease-in);

  max-height: 146px;
    max-width: 146px;
  transform: translate(0px,-100px);
  opacity: 0;
  }
.card:hover .thumb-blur img {
  filter: blur(2px);
  opacity: 1;
   max-height: 150px;
    max-width: 140px;

}
.corner {

  border: 1px solid rgba(255,0,0,1);
  width: 100px;
  height: 20px;
  position: absolute;
  text-align: center;
  background-color: white;
  top: 10px;
  right: -25px;
  transform: rotate(45deg);
  z-index: 1;
  box-shadow: 0 5px 8px rgba(0,0,0,0.3);

  .Transition();

}
.corner p {
  font-size: 17px;
  padding: 0;
  margin: 0;
  margin-top: -2px;
  color: rgba(0,0,0,0.5);
}

.card:hover .corner {
      box-shadow: 0 4px 7px rgba(0,0,0,0.2);
      background-color: rgba(217, 224, 107, 0.8);
  }
.body-card {
 background-color: rgba(255,255,255,1);
  position: absolute;
  top: 150px;
  height: 100px;
  width: 100%;
  box-shadow: 0px -10px 10px rgba(0,0,0,0.2);
  .Transition(@speed,cubic-bezier(0,.81,.38,1.37));


  z-index: 1;

}
.card:hover .body-card {
  top: 100px;
  box-shadow: 0 -4px 7px rgba(0,0,0,0.1) ;
 background-color: rgba(255,255,255,0.5);
}

.body-header {
  position: absolute;
  width: 100%;
  height: 50px;
text-align: center;
vertical-align: middle;
  padding-top : 0;
}

.body-footer {
position: absolute;
display: inline-block;
  text-align: center;
  width: 100%;
  top: 48px;
  height: 52px;
  margin: 0;
  padding: 0;
  transition: all @speed ease-in-out;
}
.card:hover .body-footer {
  top: 40px;
  height: 60px;
}
.btn-warning.btn-xs{
 transform: scale(0.9);


}

div.ex1 {
  width: 745px;
  height: 410px;
  overflow: scroll;
}
div.ex2 {
  width: 500px;
  height: 260px;
  overflow: scroll;
}


.card:hover .body-header .name {
  color: rgba(0,0,0,1);
 transform: scale(1.1);

}.card:hover .body-header p {
  color: rgba(0,0,0,0.8);
 transform: scale(1.2);
}
.footer_button {
position: absolute;
right: 14px;
bottom: 29px;
left: 15px;
background-color: white;
text-align: center;
padding-top: 0px;
}
.pen body {
	padding-top:50px;
}

/* MODAL FADE LEFT RIGHT BOTTOM */
.modal.fade:not(.in).left .modal-dialog {
	-webkit-transform: translate3d(-0%, 0, 0);
	transform: translate3d(-0%, 0, 0);
}
.modal.fade:not(.in).right .modal-dialog {
	-webkit-transform: translate3d(0%, 0, 0);
	transform: translate3d(0%, 0, 0);
}
.modal.fade:not(.in).bottom .modal-dialog {
	-webkit-transform: translate3d(0, 0%, 0);
	transform: translate3d(0, 0%, 0);
}

.modal.right .modal-dialog {
	position:absolute;
	top:0;
	right:0;
	margin:0;
}

.modal.left .modal-dialog {
	position:absolute;
	top:0;
	left:0;
	margin:0;
}

.modal.left .modal-dialog.modal-sm {
	max-width:500px;
}

.modal.left .modal-content, .modal.right .modal-content {
	min-height:100vh;
	border:0;
}

</style>
</head>

<div class="row" style="margin-bottom: -30px">
    <div class="col-xl-7 col-lg-12">

        <!--Begin::Portlet-->
        <div class="m-portlet  m-portlet--full-height exx">
            <div class="m-portlet__head" style="height: 40px;
            background-color: ghostwhite;">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Daftar Menu Utama
                        </h3>
                    </div>
                </div>
                
            </div>
            <div class="m-portlet__body">
                <div class="ex1">
                    @foreach ($menu_utama as $item_menu_utama)
                    @php
                        $img_menu_utama = "public/IMAGE_MENU_UTAMA/".$item_menu_utama->image;
                    @endphp
                    <div class="product-card">
                        <div class="card">
                            <div class="corner" ><p style="font-size:14px;margin-left:7px">{{ number_format($item_menu_utama->harga,0) }}</p>
                            </div>
                            <div class="body-card">
                                <div class="body-header">
                                <div class="name" style="margin-top: 5px">{{ $item_menu_utama->nama }}</div>
                                </div>
                                <br>
                                <div class="body-footer">
                                    <a href="#" data-name="{{ $item_menu_utama->nama }}" data-price="{{ $item_menu_utama->harga }}"
                                         class="btn btn-default btn-block add-to-cart"><span class="fa fa-cart-plus fa-lg" aria-hidden="true"></span>
                                Tambahkan </a>
                                </div>
                            </div>
                            <div class="thumb">

                            <img src="{{ $img_menu_utama }}"/>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>

            </div>

        </div>

        <!--End::Portlet-->
    </div>

    <div class="col-xl-5 col-lg-12">

        <!--Begin::Portlet-->
        <div class="m-portlet m-portlet--full-height ">
            <div class="m-portlet__head" style="height: 40px;
            background-color: ghostwhite;">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Invoice
                        </h3>
                    </div>
                </div>

            </div>
            <div class="m-portlet__body">

                <div class="ex2">
                    <table class="show-cart table">
                    </table>
                </div>
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-7">
                        <h5>Total Harga</h5>
                    </div>
                    <div class="col-md-5">

                        <span class="m-widget24__stats m--font-danger">
                            <b style="font-size: 23px">Rp. <span class="total-cart"></span>,00</b>
                        </span>

                    </div>
                </div>

                <hr>
                Tanggal Invoice : @php echo date('d F Y , h:i'); @endphp
                {{-- <button class="clear-cart btn btn-danger" style="margin-top: 10px">Hapus Menu</button> --}}
                <div class="footer_button">
                    <button data-toggle="modal" data-target="#sidebar-right" type="button" class="btn btn-danger btn-block"><span>
                    </span><b style="font-size: 23px"> <i style="font-size: 2.3rem;" class="la la-shopping-cart"></i> PAY BILLS</b></button>
                </div>

                {{-- kasir harga --}}
            </div>

        </div>

        <!--End::Portlet-->
    </div>
</div>


<!-- Sidebar Right -->
<div class="modal fade right" id="sidebar-right" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 30%" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Detail Invoice  <small> - Tanggal : @php echo date('d F Y'); @endphp </small></h4>
        </div>
        <div class="modal-body">
        <center>
           <img style="width:100px" src="public/logo-pos.png"> </img>
           <br>
           <img style="width:300px" src="public/tulisanwarung.png"> </img>
        </center>
        <br>
        <hr>
        <div class="row">
            <div class="col-md-5">
                Total items <b> (<span class="total-count"></span>)</b>
            </div>
            <div class="col-md-7">
               <b>Kode Transaksi :</b> TR-20193719
            </div>
        </div>

        <hr>
        <div class="detail-invoice">
        </div>

        <hr>
        <div class="row">
            <div class="col-md-6">
                Total yang harus dibayar
            </div>
            <div class="col-md-6">
                <div class="total-cart-detail-invoice">
                </div>
            </div>
        </div>
        <div class="footer_button">
            <button type="button" class="btn btn-primary btn-block"><span>
            </span><b style="font-size: 20px"> <i style="font-size: 2.0rem;" class="la la-print"></i> CETAK STRUK</b></button>
        </div>
        </div>
        </div>
    </div>
</div>
<!-- Nav -->
{{-- <nav class="navbar navbar-inverse bg-inverse fixed-top bg-faded">
    <div class="row">
        <div class="col">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cart">Cart (<span class="total-count"></span>)</button><button class="clear-cart btn btn-danger">Clear Cart</button></div>
    </div>
</nav> --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script>
    $ (document).ready (function () {
	$ (".modal a").not (".dropdown-toggle").on ("click", function () {
		$ (".modal").modal ("hide");
	});
});
    // ************************************************
// Shopping Cart API
// ************************************************

var shoppingCart = (function() {
  // =============================
  // Private methods and propeties
  // =============================
  cart = [];

  // Constructor
  function Item(name, price, count) {
    this.name = name;
    this.price = price;
    this.count = count;
  }

  // Save cart
  function saveCart() {
    sessionStorage.setItem('shoppingCart', JSON.stringify(cart));
  }

    // Load cart
  function loadCart() {
    cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
  }
  if (sessionStorage.getItem("shoppingCart") != null) {
    loadCart();
  }

  // =============================
  // Public methods and propeties
  // =============================
  var obj = {};

  // Add to cart
  obj.addItemToCart = function(name, price, count) {
    for(var item in cart) {
      if(cart[item].name === name) {
        cart[item].count ++;
        saveCart();
        return;
      }
    }
    var item = new Item(name, price, count);
    cart.push(item);
    saveCart();
  }
  // Set count from item
  obj.setCountForItem = function(name, count) {
    for(var i in cart) {
      if (cart[i].name === name) {
        cart[i].count = count;
        break;
      }
    }
  };
  // Remove item from cart
  obj.removeItemFromCart = function(name) {
      for(var item in cart) {
        if(cart[item].name === name) {
          cart[item].count --;
          if(cart[item].count === 0) {
            cart.splice(item, 1);
          }
          break;
        }
    }
    saveCart();
  }

  // Remove all items from cart
  obj.removeItemFromCartAll = function(name) {
    for(var item in cart) {
      if(cart[item].name === name) {
        cart.splice(item, 1);
        break;
      }
    }
    saveCart();
  }

  // Clear cart
  obj.clearCart = function() {
    cart = [];
    saveCart();
  }

  // Count cart
  obj.totalCount = function() {
    var totalCount = 0;
    for(var item in cart) {
      totalCount += cart[item].count;
    }
    return totalCount;
  }

  // Total cart
  obj.totalCart = function() {
    var totalCart = 0;
    for(var item in cart) {
      totalCart += cart[item].price * cart[item].count;
    }
    return Number(totalCart.toFixed(2));
  }

  // List cart
  obj.listCart = function() {
    var cartCopy = [];
    for(i in cart) {
      item = cart[i];
      itemCopy = {};
      for(p in item) {
        itemCopy[p] = item[p];

      }
      itemCopy.total = Number(item.price * item.count).toFixed(2);
      cartCopy.push(itemCopy)
    }
    return cartCopy;
  }
  return obj;
})();


// *****************************************
// Triggers / Events
// *****************************************
// Add item
$('.add-to-cart').click(function(event) {
  event.preventDefault();
  var name = $(this).data('name');
  var price = Number($(this).data('price'));
  shoppingCart.addItemToCart(name, price, 1);
  displayCart();
});

// Clear items
$('.clear-cart').click(function() {
  shoppingCart.clearCart();
  displayCart();
});


function displayCart() {
  var cartArray = shoppingCart.listCart();
  var output = "";
  var detail_invoice = "";
  for(var i in cartArray) {
    output += "<tr>"
      + "<td><b>" + cartArray[i].name + '</b><br>'+'Rp. '+ cartArray[i].price+ "</td>"
      + "<td style='width:180px'><div class='input-group'><button class='minus-item input-group-addon btn btn-primary' data-name=" + cartArray[i].name + ">-</button>"
      + "<input type='number' class='item-count form-control' data-name='" + cartArray[i].name + "' value='" + cartArray[i].count + "'>"
      + "<button class='plus-item btn btn-primary input-group-addon' data-name=" + cartArray[i].name + ">+</button></div></td>"
      + "<td><button class='delete-item btn btn-danger' data-name=" + cartArray[i].name + "><i class='la la-trash'></i></button></td>"
      + " = "
      + "<td><b style='font-size:15px;color:gray'>" + cartArray[i].total + "</b></td>"
      +  "</tr>";
      detail_invoice += "<tr>"
      + "<td><b>" + cartArray[i].name + '</b></td><td>&nbsp&nbsp&nbsp</td><td>'+'Rp. '+ cartArray[i].price+ "</td><td>&nbsp&nbsp&nbsp</td>"
      + "<td style='width:50px'><div class='input-group'>"
      + "<b class='item-count' data-name='" + cartArray[i].name + "'>"+' x ' +cartArray[i].count+"</b>"
      + "</div></td>"
      + "<td></td>"
      + " = "
      + "<td><b>Rp. " + cartArray[i].total + "</b></td>"
      +  "</tr>";
  }
  $('.show-cart').html(output);
  $('.detail-invoice').html('<tr style="color:#999aa3"><th># items</th><th>&nbsp&nbsp&nbsp</th><th>harga</th>'+
  '<th>&nbsp&nbsp&nbsp</th>'+'<th>jumlah</th><th>&nbsp&nbsp&nbsp</th><th>total</th></tr>'  +detail_invoice);
  $('.total-cart').html(shoppingCart.totalCart());
  $('.total-cart-detail-invoice').html('<h5 style="color:red">Rp. ' + '<b>' +shoppingCart.totalCart()  +',00</b> </h5>' );
  $('.total-count').html(shoppingCart.totalCount());
}

// Delete item button

$('.show-cart').on("click", ".delete-item", function(event) {
  var name = $(this).data('name')
  shoppingCart.removeItemFromCartAll(name);
  displayCart();
})


// -1
$('.show-cart').on("click", ".minus-item", function(event) {
  var name = $(this).data('name')
  shoppingCart.removeItemFromCart(name);
  displayCart();
})
// +1
$('.show-cart').on("click", ".plus-item", function(event) {
  var name = $(this).data('name')
  shoppingCart.addItemToCart(name);
  displayCart();
})

// Item count input
$('.show-cart').on("change", ".item-count", function(event) {
   var name = $(this).data('name');
   var count = Number($(this).val());
  shoppingCart.setCountForItem(name, count);
  displayCart();
});

displayCart();

$(document).ready(function($){

});
</script>

@endsection
