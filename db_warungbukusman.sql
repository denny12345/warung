-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Feb 2021 pada 11.30
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_warungbukusman`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tm_menu`
--

CREATE TABLE `tm_menu` (
  `id` int(11) NOT NULL,
  `nama` text NOT NULL,
  `harga` varchar(100) NOT NULL,
  `image` varchar(200) NOT NULL,
  `tipe_menu` varchar(200) NOT NULL,
  `keuntungan` varchar(50) NOT NULL,
  `status_aktif` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tm_menu`
--

INSERT INTO `tm_menu` (`id`, `nama`, `harga`, `image`, `tipe_menu`, `keuntungan`, `status_aktif`, `created_at`, `updated_at`) VALUES
(8, 'Tahu', '1000', 'tahu-kuning-goreng-kalisari-banyumas-foto-resep-utama.jpg', 'MENU PELENGKAP', '500', 0, '2021-02-13 09:51:24', '2021-02-13 10:15:23'),
(7, 'Nasi Rawon', '20000', '030607000_1467613172-rawon-setan.jpg', 'MENU UTAMA', '5000', 0, '2021-02-13 09:46:58', '2021-02-13 09:49:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tt_kas`
--

CREATE TABLE `tt_kas` (
  `id` int(11) NOT NULL,
  `generate_kode_kas` text NOT NULL,
  `jumlah_kas` int(50) NOT NULL,
  `keterangan` text NOT NULL,
  `total_kas_terakhir` int(50) NOT NULL,
  `status` text NOT NULL,
  `users_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tt_kas`
--

INSERT INTO `tt_kas` (`id`, `generate_kode_kas`, `jumlah_kas`, `keterangan`, `total_kas_terakhir`, `status`, `users_id`, `created_at`, `updated_at`) VALUES
(0, '12345', 10000000, 'Okw', 0, 'Pemasukan', 0, '2021-02-13 16:06:18', '0000-00-00 00:00:00'),
(1, '123456', 10000000, 'Pemasukan', 100000, 'Pemasukan', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tt_kasir`
--

CREATE TABLE `tt_kasir` (
  `id` int(11) NOT NULL,
  `generate_code_pembelian` varchar(200) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `tgl_transaksi` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `role`, `email`, `email_verified_at`, `password`, `token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'denny', 'denny', 1, 'masterdenny12@gmail.com', NULL, '$2y$10$5evUBdJXrFT6HMeC3zYJr.mlwjusAVO3ys/FkiDxO10gZL2.ha6Ai', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tm_menu`
--
ALTER TABLE `tm_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tt_kas`
--
ALTER TABLE `tt_kas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tt_kasir`
--
ALTER TABLE `tt_kasir`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tm_menu`
--
ALTER TABLE `tm_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `tt_kasir`
--
ALTER TABLE `tt_kasir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
